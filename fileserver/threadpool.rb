class ThreadPool
#basic threadpool class
  require 'thread'

  def initialize(pool_size)
    puts 'Threadpool created'
    @pool_size = pool_size
    @jobs = Queue.new
    puts "Adding #{@pool_size} threads...\n\n"
    @pool = Array.new(@pool_size) do |i|
      Thread.new do
        Thread.current[:id] = i
        catch(:exit) do
          loop do
            job, args = @jobs.pop
            job.call(*args)
          end
        end
      end
    end
  end

  def schedule(*args, &block)
    @jobs << [block, args]
  end

  def shutdown
    Thread.list.each do |thread|
      thread.exit
    end
  end

end