class Server
#chunk for large files
  SIZE = 1024 * 1024 

  require "socket"
  require_relative "thread_pool"

  attr_reader :server
  attr_reader :port

  
  def initialize(port, size)
    @port = port
    @server = "localhost"
    @server = TCPServer.new @port
    @pool = ThreadPool.new(size.to_i) #create pool
    start
  end

  
  def start_down(fname, client)
    begin
      file = open('files/'<<fname, 'rb')
      if file
        client.puts "Found file"
        chunk_counter = 0
        while chunk = file.read(SIZE)
          chunk_counter += 1
        end

        puts "Thread: #{Thread.current[:id]} -- File #{fname} has #{chunk_counter} chunks"
        client.puts chunk_counter #inform client of chunks to receive
        #reset pointers
        file.rewind

        begin
          for i in 1..chunk_counter
            chunk = file.read(SIZE)
            client.write(chunk)
          end
        puts "Thread #{Thread.current[:id]} -- Download Sent!"
        rescue Errno::EPIPE
        puts "Thread #{Thread.current[:id]} -- Connection lost ---"
        end
        file.close
      end
    rescue
      puts "Thread #{Thread.current[:id]} -- File doesn't exist: #{fname}"
      client.puts "SERVER_FAILED: FILE DOES NOT EXIST"
    end

  end

#upload receiver
def receive_up(fname, client)

    file = open('files/'<<fname, 'w')

    chunk_counter = client.gets.chomp.to_i #get chunk count to receive from client
    puts "Thread #{Thread.current[:id]} -- Preparing to receive: #{chunk_counter} chunks!"

    for i in 1..chunk_counter
      chunk = client.read(SIZE)
      file.write(chunk)
      puts "Thread #{Thread.current[:id]} -- Received/wrote chunk #{i}"
    end

    puts "Thread #{Thread.current[:id]} -- Upload Received!"

    file.close

  end

#Parse client messages
  def listen_clients(client, message)
      command = message.split(' ')[0]
      params = message.split(' ')[1]
      puts "Thread #{Thread.current[:id]} -- #{message}"

      if command == 'HELO'
        client.puts "#{message}\nIP:#{@server_ip}\nPort:#{@port}\n Student Num:"
      end
      if command == "CLIENT_FAILED"
        puts "Thread #{Thread.current[:id]} -- ERROR: Client side errors"
      end

      if command == "UPLOAD:"
        receive_up(params, client)
      end
      if command == "DOWNLOAD:"
        start_down(params, client)
      end
      client.close
  end

  def start

    loop do
      client = @server.accept
      #handoff new client connection to a thread in threadpool
      @pool.schedule do
        message = client.gets.chomp #exe message
        listen_clients(client, message)

        client.puts "File Server: Connected!"
      end
    end
  end
end
s = Server.new(ARGV[0], ARGV[1])